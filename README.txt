README file for routine to insert intersitital solute atoms in 
the largest voronoi volumes of a given configuration.
Author: Pablo Piaggi
Should you have any questions or suggestions, please send an 
email to: ppiaggi@hotmail.com

Instructions:
1) Download files with the instruction: git clone https://ppiaggi@bitbucket.org/ppiaggi/voronoiinterstitial.git VoronoiInterstitial 
2) Compile src. Just type make.
3) Move file main.x to a separate folder together with the file
in.voronoi and input.
4) Edit the last two files with data for your simulation (masses, 
 potential, etc).
5) Compile LAMMPS with the Voro++ package. Move the resulting
executable file to the desired folder and change its name to
lmp_voro .
6) With the files main.x, lmp_voro, in.voronoi, input, and your
 potential file you are ready for executing ./main.x .

Parameters in input file:
IMPORTANT: Do not change the order of the parameters! It may
lead to errors.
1) solute_fraction = Desired fraction (i.e Nsolute/(Nmatrix+Nsolute))
2) innerCutoff and outerCutoff = Solute insertion is done in a 
random way within the voronoi volume but if matrix and solute
atoms are too near it may lead to problems. This is why cutoff
values are specified. You may want to keep the default values.
3) mass1 and mass2 = Masses for your simulation. mass1 is for the 
matrix and mass2 for the solute.
4) data_file = Name of the data file in which you desire to insert 
solute atoms. No quotes.
5) make_dump = "yes" for the first run, "no" for others. No quotes.

