MODULE variables

IMPLICIT NONE

INTEGER :: num_atoms, time_step, solute_number, matrix_number
REAL*8, ALLOCATABLE, DIMENSION(:,:) :: pos, solute_pos, matrix_pos
REAL*8, ALLOCATABLE, DIMENSION(:) :: voro_vol
INTEGER, ALLOCATABLE, DIMENSION(:) :: atom_type, atom_index
REAL*8, DIMENSION(3,2) :: box_bounds
REAL :: time_begin, time_end
REAL*8 :: solute_fraction, innerCutoff, outerCutoff, mass1, mass2
CHARACTER(len=60) :: data_file
INTEGER :: make_dump

END MODULE variables
