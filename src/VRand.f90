SUBROUTINE VRand(vec)

IMPLICIT NONE

REAL*8 :: vec(3), s, x, y

s = 2.
DO WHILE (s > 1.)
        CALL RANDOM_NUMBER(x)
        x = 2. * x -1.
        CALL RANDOM_NUMBER(y)
        y = 2. * y -1.
        s = x**2 + y **2
END DO
vec(3)= 1.- 2.*s
s= 2. * SQRT (1.-s)
vec(1)= s*x
vec(2)= s*y

END SUBROUTINE VRand

