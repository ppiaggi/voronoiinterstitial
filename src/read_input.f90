SUBROUTINE read_input(file_name)

USE variables

IMPLICIT NONE

CHARACTER(len=20) :: file_name

INTEGER :: i, j, IOstatus
CHARACTER(len=30) :: line, dummy_line
REAL*8 :: dummy

OPEN(1,file=TRIM(file_name))

IOstatus=0

DO WHILE ( IOstatus==0 )
   READ(1,*,IOSTAT=IOstatus) line, dummy
   IF (TRIM(line)=='solute_fraction') THEN
      solute_fraction=dummy
      READ(1,*,IOSTAT=IOstatus) line, dummy
   ENDIF
   IF (TRIM(line)=='innerCutoff') THEN
      innerCutoff=dummy
      READ(1,*,IOSTAT=IOstatus) line, dummy
   ENDIF
   IF (TRIM(line)=='outerCutoff') THEN
      outerCutoff=dummy
      READ(1,*,IOSTAT=IOstatus) line, dummy
   ENDIF
   IF (TRIM(line)=='mass1') THEN
      mass1=dummy
      READ(1,*,IOSTAT=IOstatus) line, dummy
   ENDIF
   IF (TRIM(line)=='mass2') THEN
      mass2=dummy
      READ(1,*,IOSTAT=IOstatus) line, dummy_line
   ENDIF
   IF (TRIM(line)=='data_file') THEN
      data_file=dummy_line
      READ(1,*,IOSTAT=IOstatus) line, dummy_line
   ENDIF
   IF (TRIM(line)=='make_dump') THEN
      IF (TRIM(dummy_line)=='yes') THEN
        make_dump=1
      ELSEIF (TRIM(dummy_line)=='no') THEN
        make_dump=0
      ENDIF
      READ(1,*,IOSTAT=IOstatus) line, dummy
   ENDIF
END DO

END SUBROUTINE read_input
