SUBROUTINE create_atoms(atom_pos, max_radius)

USE variables

IMPLICIT NONE

REAL*8 :: unit_vector(3), atom_pos(3), max_radius, radius_factor


CALL VRand(unit_vector)

CALL RANDOM_NUMBER(radius_factor)
DO WHILE (radius_factor<innerCutoff .OR. radius_factor>outerCutoff)
        CALL RANDOM_NUMBER(radius_factor)
END DO

atom_pos=atom_pos+unit_vector*max_radius*radius_factor

END SUBROUTINE create_atoms
