SUBROUTINE write_file(file_name)

USE variables
IMPLICIT NONE

CHARACTER(len=20) :: file_name

CHARACTER(len=120) :: line
CHARACTER(len=20), EXTERNAL :: descriptor
INTEGER :: i, contador

!OPEN(1,FILE=TRIM(file_name),STATUS='OLD',POSITION='APPEND')
!OPEN(1,FILE=TRIM(file_name), STATUS='NEW')
OPEN(1,FILE=TRIM(file_name))

WRITE(*,'(A34)') "Starting to write output data file"

!Escribe comentarios
line='LAMMPS data file'
WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
WRITE(1,*) 
!Numero de atomos
WRITE(line,'(I6,A6)') num_atoms, " atoms"
WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
!Numero de tipos
WRITE(line,*) "2 atom types"
WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
WRITE(1,*) 
!Box bounds
WRITE(line,'(2ES23.16,A8)') box_bounds(1,:), " xlo xhi"
WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
WRITE(line,'(2ES23.16,A8)') box_bounds(2,:), " ylo yhi"
WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
WRITE(line,'(2ES23.16,A8)') box_bounds(3,:), " zlo zhi"
WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
WRITE(1,*) 
!Masses
WRITE(line,*) "Masses"
WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
WRITE(1,*) 

WRITE(line,'(A2,F7.3)') "1 ", mass1
WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)

WRITE(line,'(A2,F7.3)') "2 ", mass2
WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
WRITE(1,*) 

!Atoms

WRITE(line,*) "Atoms"
WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
WRITE(1,*) 

DO i=1,num_atoms
        WRITE(line,'(I6,I3,3ES24.16,3I3)') i, atom_type(i), pos(:,i), 0, 0, 0
        WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
END DO
WRITE(1,*) 

!Velocities
WRITE(line,*) "Velocities"
WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
WRITE(1,*) 

DO i=1,num_atoms
       WRITE(line,'(I6,3ES24.16)') i, 0.d0, 0.d0, 0.d0
        WRITE(1,TRIM(descriptor(line))) ADJUSTL(line)
END DO


CLOSE(1)

WRITE(*,'(A33)') "Finished writing output data file"

END SUBROUTINE write_file

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FUNCTION descriptor(line)

IMPLICIT NONE

CHARACTER(len=120) :: line
CHARACTER(len=20) :: descriptor
INTEGER :: char_len

char_len=LEN_TRIM(line)
WRITE(descriptor,*) char_len+1
descriptor=TRIM('(A' // ADJUSTL(descriptor)) // ')'

END FUNCTION descriptor
