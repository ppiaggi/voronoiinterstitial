SUBROUTINE read_file(file_name)

USE variables

IMPLICIT NONE

CHARACTER(len=20) :: file_name

INTEGER :: i, j, IOstatus
CHARACTER(len=60) :: line1, line2, line
INTEGER :: dummy1, dummy2, num_tags
INTEGER, DIMENSION(10) :: tags

OPEN(1,file=TRIM(file_name))

IOstatus=0

DO WHILE ( IOstatus==0 )
   READ(1,*,IOSTAT=IOstatus) line1, line2
   IF (TRIM(line1)=='ITEM:' .AND. TRIM(line2)=='TIMESTEP') THEN
      READ(1,*) time_step
      READ(1,*,IOSTAT=IOstatus) line1, line2
   ENDIF
   IF (TRIM(line1)=='ITEM:' .AND. TRIM(line2)=='NUMBER') THEN
      READ(1,*) matrix_number
      CALL alloc_arrays(1,matrix_number,matrix_number)
      READ(1,*,IOSTAT=IOstatus) line1, line2
   ENDIF
   IF (TRIM(line1)=='ITEM:' .AND. TRIM(line2)=='BOX') THEN
           DO i=1,3
                READ(1,*) box_bounds(i,:)
           END DO
           READ(1,*,IOSTAT=IOstatus) line1, line2
   ENDIF
   IF (TRIM(line1)=='ITEM:' .AND. TRIM(line2)=='ATOMS') THEN
           DO i=1,matrix_number
                READ(1,*) pos(:,i), voro_vol(i)
           END DO
           READ(1,*,IOSTAT=IOstatus) line1, line2
   ENDIF
END DO
CLOSE(1)

END SUBROUTINE read_file
