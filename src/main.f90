!Program for inserting atoms within the largest voronoi
!volumes of a given configuration.

PROGRAM main

USE variables
USE qsort_c_module

IMPLICIT NONE

INTEGER :: k
REAL*8, DIMENSION(3) :: atom_pos
REAL*8 :: radius
REAL*8, PARAMETER :: Pi=3.14159265359
CHARACTER(len=20) :: parameters_file_name
CHARACTER(len=20) :: input_file_name
CHARACTER(len=20) :: output_file_name, lammps_input_file
CHARACTER(len=60) :: line
CHARACTER(len=200) :: line2

CALL CPU_TIME(time_begin)

parameters_file_name='input'
input_file_name='dump_voronoi.0'
output_file_name='voronoi.data'

CALL read_input(parameters_file_name)

!data_file='W.relax.data.119000'
!lammps_input_file="in.voro"

IF (make_dump==1) THEN
        WRITE(*,'(A42)') "Running LAMMPS for finding Voronoi volumes"
        WRITE(*,'(A24)') "(returns 0 if succesful)"
        CALL SYSTEM("./lmp_voro -v datafile " // TRIM(ADJUSTL(data_file)) // " < in.voronoi > log.lammps")
        CALL SYSTEM("echo $?")
ELSEIF (make_dump==0) THEN
        WRITE(*,'(A43)') "Voronoi dump file already made. Skipping..."
ENDIF

CALL read_file(input_file_name)

CALL set_params

!Sorting routine
CALL QsortC(voro_vol,atom_index)

CALL alloc_arrays(2,num_atoms,matrix_number)

DO k=matrix_number+1,num_atoms
        atom_type(k)=2
END DO

CALL RANDOM_SEED()

DO k=1,solute_number
        atom_pos=pos(:,atom_index(matrix_number-k+1))
        radius=(3.*voro_vol(matrix_number-k+1)/(4*Pi))**(1./3.)
        call create_atoms(atom_pos, radius)
        !WRITE(*,'(4F16.8)') atom_pos, radius, voro_vol(W_number-k+1)
        pos(:,matrix_number+k)=atom_pos
END DO

CALL write_file(output_file_name)

CALL CPU_TIME(time_end)

WRITE(*,'(A17,F10.5,A2)') "Time elapsed is: ", time_end-time_begin, " s"

END PROGRAM main
