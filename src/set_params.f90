SUBROUTINE set_params

USE variables
IMPLICIT NONE

INTEGER :: k 

WRITE(*,'(A19,F15.5)') "Solute fraction is ", solute_fraction

solute_number=solute_fraction*matrix_number/(1-solute_fraction)

WRITE(*,'(A26,I10)') "Number of solute atoms is ", solute_number
WRITE(*,'(A26,I10)') "Number of matrix atoms is ", matrix_number

num_atoms=matrix_number+solute_number

WRITE(*,'(A25,I10)') "Total number of atoms is ", num_atoms

atom_type=1

DO k=1,matrix_number
        atom_index(k)=k
END DO


END SUBROUTINE set_params


