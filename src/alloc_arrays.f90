SUBROUTINE alloc_arrays(stage,array_size,old_array_size)

USE variables

IMPLICIT NONE

INTEGER :: array_size, stage, old_array_size
REAL*8, ALLOCATABLE :: tempR(:,:)
INTEGER, ALLOCATABLE :: tempI(:)

IF (stage==1) THEN
        ALLOCATE(pos(3,array_size))
        ALLOCATE(voro_vol(array_size))
        ALLOCATE(atom_type(array_size))
        ALLOCATE(atom_index(array_size))
        !ALLOCATE(tempR(3,array_size))
        !ALLOCATE(tempI(array_size))
ELSEIF (stage==2) THEN
        ALLOCATE(tempR(3,old_array_size))
        tempR=pos
        DEALLOCATE(pos)
        !CALL MOVE_ALLOC(pos,tempR) 
        ALLOCATE(pos(3,array_size))
        pos(:,1:old_array_size)=tempR
        DEALLOCATE(tempR)
        !CALL
        !MOVE_ALLOC(atom_type,tempI) 
        ALLOCATE(tempI(old_array_size))
        tempI=atom_type
        DEALLOCATE(atom_type)
        ALLOCATE(atom_type(array_size))
        atom_type(1:old_array_size)=tempI
        DEALLOCATE(tempI)
        !CALL MOVE_ALLOC(pos,tempR) 
        !ALLOCATE(pos(3,array_size))
        !pos(:,1:old_array_size)=tempR
        !DEALLOCATE(tempR)
        !CALL MOVE_ALLOC(atom_type,tempI) 
        !ALLOCATE(atom_type(array_size))
        !atom_type(1:old_array_size)=tempI
        !DEALLOCATE(tempI)
ENDIF

END SUBROUTINE alloc_arrays
